#include <stdio.h>
int merge_sort(int arr[],int low,int high);
int merge(int arr[],int l,int m,int h);
void common(int part1_x, int part1_y, int arr1[], int n1, int arr2[], int n2);
int find_length(char input[]);
void menu();
void Capital(char *input , int size , int start);
int findFactors(int arr1[],int arr2[],int factors,int indexOfarr1,int indexOfArr2);
void assignZero(int arr[]);
void evalFunctions(int n);
double formula(int number ,int digit);
int findDigit(int number);
int main(){

	menu();





}


void menu(){
	char input[100];
	int part1_x;
	int part1_y;
	int choice = -1;
	int length;
	int arr[10];
	int arr1[100];
	int arr2[100];
	int factor;
	int i;
	int capital_helper;
	int n;
	int number = 0 , counter = 0 , temp;
	assignZero(arr1);
	assignZero(arr2);

	while(choice != 6){

	printf("1) Multipliers\n2)Sorting\n3)Formula\n4)Equal-Not Equal\n5)Capital Number\n6)Exit\n---Choice :");
	scanf("%d",&choice);
	switch (choice){
			case 1 :
				
				printf("\n\n\n");
				printf("Please enter the first number\n");
				scanf("%d",&part1_x);
				printf("Please enter the second number\n");
				scanf("%d",&part1_y);
				common(part1_x,part1_y,arr1,0,arr2,0);
					
				factor=findFactors(arr1,arr2,1,0,0);
					

				printf("Result:   %d\n",factor);
			
				
				printf("\n\n\n");
				break;
			case 2 :
				printf("\n\n\n");
				printf("Enter the length of the list :");
				scanf("%d",&length);
				arr[length];
				for (i = 0; i < length; ++i)
				{
					scanf("%d",&arr[i]);
				}

				merge_sort(arr,0,length-1);

				printf("Sorted List : " );
				for (i = 0; i < length; ++i)
				{
					printf("%d ",arr[i]);
				}
				printf("\n\n\n");


				break;
			case 3:
				printf("\n\n\n");
				printf("Input : ");
				scanf("%d",&n);
				printf("Output: %d ",n );
				evalFunctions(n);
				printf("\n\n\n");
				break;			
			case 4:
				printf("\n\n\n");
				printf("Enter the number : ");
				scanf("%d",&number);
	
				counter = findDigit(number);

				formula(number,counter);
				printf("\n\n\n");	
				break;
			case 5:	
				printf("\n\n\n");
				printf("Enter the string without any space\n");
				scanf("%s",input);
				find_length(input);
				printf("\n\n\n");
			case 6:
				break;
				default :
					printf("Please enter between (0-6)\n");
					break;
	}


	}




}

void common(int part1_x, int part1_y, int arr1[], int n1, int arr2[], int n2){
   

    int count_2=2;     //This is for part one to find multipliers of numbers
    int count_1=2;

    if (part1_x==1){

   		if(part1_y == 1)
   			return;
   

   	while (part1_y%count_2 != 0)
        count_2++;
    
    arr1[n1]=count_2;                        // and assign the multipliers into an array
    common(part1_x,part1_y/count_2,arr1,n1+1,arr2,n2);


    }
       
    else if(part1_x>1){


    while (part1_x%count_1 != 0)
        count_1++;
    

    arr2[n2]=count_1;
    common(part1_x/count_1,part1_y,arr1,n1,arr2,n2+1);

    }

}

int findFactors(int arr1[],int arr2[],int factors,int indexOfarr1,int indexOfArr2){


	if(arr1[indexOfarr1]==arr2[indexOfArr2] && arr1[indexOfarr1]!=0 && arr2[indexOfArr2]!=0){		
		factors*=arr1[indexOfarr1];
		indexOfarr1++;
		indexOfArr2++;

	
	}else if(arr1[indexOfarr1]>arr2[indexOfArr2] ){
		
		indexOfArr2++;
	
	}else if(arr1[indexOfarr1]<arr2[indexOfArr2]){
		indexOfarr1++;
	
	}


	if(indexOfarr1 < 100 && indexOfArr2 < 100 && arr1[indexOfarr1]!=0 && arr2[indexOfArr2]!=0){
	
		factors=findFactors(arr1,arr2,factors,indexOfarr1,indexOfArr2);
	}
	
	return factors;         //find the same numbers and multiple the numbers and return
}

void assignZero(int arr[]){

	int i;                //assigning to zeros for helping checking
	for(i=0; i<100; i++)
		arr[i]=0;
}
int merge_sort(int arr[],int low,int high)
{
  int mid;								//this is merge sort
  if(low<high)
  {
    mid=(low+high)/2;
  										//This is for divide the array
    merge_sort(arr,low,mid);
    merge_sort(arr,mid+1,high);
   											//after that combine
    merge(arr,low,mid,high);
  }
  
  return 0;
}

int merge(int arr[],int l,int m,int h)
{
  int arr1[10],arr2[10];  					// combining the array in this func
  int n1,n2,i,j,k;
  n1=m-l+1;
  n2=h-m;

  for(i=0;i<n1;i++)							//combining
    arr1[i]=arr[l+i];
  for(j=0;j<n2;j++)
    arr2[j]=arr[m+j+1];

  arr1[i]=9999;  
  arr2[j]=9999;

  i=0;j=0;
  for(k=l;k<=h;k++) 
  {
    if(arr1[i]<=arr2[j])
      arr[k]=arr1[i++];
    else
      arr[k]=arr2[j++];
  }
  
  return 0;
}
void evalFunctions(int n) {         //This is for part 3
	if (n < 2) {
		return;
	}
	if ((n % 2) == 0) {							//we need if and else to do this recursively	
		printf("%d ", (n/2));
		evalFunctions(n/2);
	}
	else {
		printf("%d ", ((3 * n) + 1));
		evalFunctions(((3 * n) + 1));
	}
}


double formula(int number ,int digit){  //THIS IS FOR PART 4

	double toplam ;
	int temp = 1;
	int i;

	if(number < 10){             //if number is smaller than 10 then return the pow of number
		
	
		for(i = 0 ; i< digit ; i++)
			temp *= number;

		
		return temp;
	}
	for(i=0;i<digit;i++)
		temp *= number%10;
	toplam = temp;
	toplam += formula(number/10,digit);

	if(findDigit(number) == digit){
		if(number == toplam){
			printf("Input : %d \n",number );
			printf("Output : Equal\n");
		}else{
			printf("Input : %d \n",number );
			printf("Output : Not Equal\n");
		}
	}

	return toplam;

}
								//PART 4 HELPER FUNC
int findDigit(int number){      //THIS IS HELPER FUNCTION FOR FINDING HOW MANY DIGITS THIS NUMBER

	int counter = 0;

	while(number > 10){

		number = number / 10;
		counter++;
	}
	counter++;	

	return counter;
}
								//PART 5 HELPER FUNC
int find_length(char input[]){  //THIS IS FOR HELPER FUNCTIOM FINDING THE SIZE OF ARRAY
	int i;

	for(i=0;input[i]!='\0';i++);

		
	//	printf("%d\n",i );
	Capital(input,i,0);

	
}
void Capital(char input[] , int size , int start){ //THIS IS FOR PART 5

	if(input[start] >= 65 && input[start] <= 90){    //if any capital write this char and quit from recursive
		printf("Output : %c\n",input[start]);
		return;
	}

	else if(start > size-1){						
			printf("There is no capital\n");
			return;
	}
	
	Capital(input,size,start+1);



}









